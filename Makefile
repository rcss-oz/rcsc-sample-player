CFLAGS=$(shell librcsc-config --cflags)
LIBS=$(shell librcsc-config --libs) -lrcsc_net -lrcsc_time
all:  sample_player

sample_player: main.cc
	g++ $(CFLAGS) $(LIBS) main.cc -o sample_player

clean:
	-rm -rf *.o sample_player
