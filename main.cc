#include <rcsc/common/basic_client.h>
#include <rcsc/param/cmd_line_parser.h>
#include <rcsc/player/player_agent.h>
#include <rcsc/param/param_map.h>

#include <iostream>
#include <cstdlib> // exit
#include <cerrno> // errno
#include <cstring> // strerror
#include <csignal> // sigaction


class SamplePlayer
    : public rcsc::PlayerAgent
{
    /*
    private:

        Communication::Ptr M_communication;

        FieldEvaluator::ConstPtr M_field_evaluator;
        ActionGenerator::ConstPtr M_action_generator;
    */
    public:

        SamplePlayer();

        virtual
        ~SamplePlayer();

    protected:

        /*!
          You can override this method.
          But you must call PlayerAgent::initImpl() in this method.
        */
        virtual
        bool initImpl( rcsc::CmdLineParser & cmd_parser );

        //! main decision
        virtual
        void actionImpl();

        //! communication decision
        virtual
        void communicationImpl();

        virtual
        void handleActionStart();
        virtual
        void handleActionEnd();

        virtual
        void handleServerParam();
        virtual
        void handlePlayerParam();
        virtual
        void handlePlayerType();

        /*
        virtual
        FieldEvaluator::ConstPtr createFieldEvaluator() const;

        virtual
        ActionGenerator::ConstPtr createActionGenerator() const;
        */

    private:

        bool doPreprocess();
        bool doShoot();
        bool doForceKick();
        bool doHeardPassReceive();

        /*
    public:
        virtual
        FieldEvaluator::ConstPtr getFieldEvaluator() const;
        */
};

using namespace rcsc;

SamplePlayer::SamplePlayer()
    : PlayerAgent()
{
    std::cout << "SamplePlayer::SamplePlayer()";
}

SamplePlayer::~SamplePlayer()
{
    std::cout << "SamplePlayer::~SamplePlayer()";
}

bool
SamplePlayer::initImpl( CmdLineParser & cmd_parser )
{
    std::cout << "SamplePlayer::initImpl()";

    bool result = PlayerAgent::initImpl( cmd_parser );

    rcsc::ParamMap my_params( "Additional options" );
#if 0
    std::string param_file_path = "params";
    param_map.add()
        ( "param-file", "", &param_file_path, "specified parameter file" );
#endif

    cmd_parser.parse( my_params );

    if ( cmd_parser.count( "help" ) > 0 )
    {
        my_params.printHelp( std::cout );
        return false;
    }

    if ( cmd_parser.failed() )
    {
        std::cerr << "player: ***WARNING*** detected unsuppprted options: ";
        cmd_parser.print( std::cerr );
        std::cerr << std::endl;
    }

    if ( ! result )
    {
        return false;
    }

    return true;
}

void
SamplePlayer::actionImpl()
{
    std::cout << "SamplePlayer::actionImpl()";
}

void
SamplePlayer::handleActionStart()
{
    std::cout << "SamplePlayer::handleActionStart()";
}

void
SamplePlayer::handleActionEnd()
{
    std::cout << "SamplePlayer::handleActionEnd()";
}

void
SamplePlayer::handleServerParam()
{
    std::cout << "SamplePlayer::handleServerParam()";
}
void
SamplePlayer::handlePlayerParam()
{
    std::cout << "SamplePlayer::handlePlayerParam()";
}

void
SamplePlayer::handlePlayerType()
{
    std::cout << "SamplePlayer::handlePlayerType()";
}

void
SamplePlayer::communicationImpl()
{
    std::cout << "SamplePlayer::communicationImpl()";
}

bool
SamplePlayer::doPreprocess()
{
    std::cout << "SamplePlayer::doPreprocess()";
    return true;
}

bool
SamplePlayer::doShoot()
{
    std::cout << "SamplePlayer::doShoot()";
    return true;
}

bool
SamplePlayer::doForceKick()
{
    std::cout << "SamplePlayer::doForceKick()";
    return true;
}

bool
SamplePlayer::doHeardPassReceive()
{
    std::cout << "SamplePlayer::doHeardPassReceive()";
    return true;
}

/*
FieldEvaluator::ConstPtr
SamplePlayer::getFieldEvaluator() const
{
    return M_field_evaluator;
}

FieldEvaluator::ConstPtr
SamplePlayer::createFieldEvaluator() const
{
    return FieldEvaluator::ConstPtr( new SampleFieldEvaluator );
}

ActionGenerator::ConstPtr
SamplePlayer::createActionGenerator() const
{
    return ActionGenerator::ConstPtr( g );
}
*/

namespace {


    SamplePlayer agent;

/*-------------------------------------------------------------------*/
    void
    sig_exit_handle( int )
    {
        std::cerr << "Killed. Exiting..." << std::endl;
        agent.finalize();
        std::exit( EXIT_FAILURE );
    }

}
using namespace std;

int main(int argc, char *argv[]) {
    struct sigaction sig_action;
    sig_action.sa_handler = &sig_exit_handle;
    sig_action.sa_flags = 0;
    if ( sigaction( SIGINT, &sig_action , NULL ) != 0
         || sigaction( SIGTERM, &sig_action , NULL ) != 0
         || sigaction( SIGHUP, &sig_action , NULL ) != 0 )
        /*if ( signal(SIGINT, &sigExitHandle) == SIG_ERR
          || signal(SIGTERM, &sigExitHandle) == SIG_ERR
          || signal(SIGHUP, &sigExitHandle) == SIG_ERR )*/
    {
        std::cerr << __FILE__ << ": " << __LINE__
                  << ": could not set signal handler: "
                  << std::strerror( errno ) << std::endl;
        std::exit( EXIT_FAILURE );
    }

    rcsc::BasicClient client;

    if ( ! agent.init( &client, argc, argv ) )
    {
        return EXIT_FAILURE;
    }

    client.run( &agent );

    return EXIT_SUCCESS;
}
